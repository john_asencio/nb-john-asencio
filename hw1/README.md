helpful git commands

git clone
–Fetch a copy of a remote repository

•git add
–Add a new file and/or directory to local repository

•git commit
–Commit changes to local repository

•git push
–Merge changes from local repository to a remote one
–Implicitly assumes "origin" (place that you retrieved repo
from) and "master" (branch)

•git pull
–Merge changes from remote repository to your local one
–Implicitly assumes "origin" (place that you retrieved repo
from) and "master" (branch)
